Source: ztree
Section: lisp
Priority: optional
Maintainer: Debian Emacsen Team <debian-emacsen@lists.debian.org>
Uploaders: Lev Lamberov <dogsleg@debian.org>
Build-Depends: debhelper (>= 10),
               dh-elpa
Standards-Version: 3.9.8
Homepage: https://github.com/fourier/ztree
Vcs-Browser: https://salsa.debian.org/emacsen-team/ztree
Vcs-Git: https://salsa.debian.org/emacsen-team/ztree.git

Package: elpa-ztree
Architecture: all
Depends: ${elpa:Depends},
         ${misc:Depends},
         emacs
Recommends: emacs (>= 46.0)
Enhances: emacs,
          emacs24,
          emacs25
Built-Using: ${misc:Built-Using}
Description: text mode directory tree
 ztree is a project dedicated to implementation of several text-tree
 applications inside GNU Emacs. It consists of 2 subprojects: ztree-diff and
 ztree-dir.
 .
 ztree-diff is a directory-diff tool for Emacs inspired by commercial tools
 like Beyond Compare or Araxis Merge. It supports showing the difference
 between two directories; calling Ediff for not matching files, copying
 between directories, deleting file/directories, hiding/showing equal
 files/directories.
 .
 ztree-dir is a simple text-mode directory tree for Emacs.
